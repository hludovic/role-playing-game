//
//  Weapon.swift
//  rolePlayingGame
//
//  Created by Ludovic HENRY on 11/12/2019.
//  Copyright © 2019 Ludovic HENRY. All rights reserved.
//

import Foundation

/// A weapon used ins this game.
class Weapon {
    var name: String
    var attackPoints: Int
    /// Returns a String that contains all the information about the weapon.
    var infos: String {
        return "[🗡\(name) \(attackPoints)AP]"
    }
    
    /// Initializes a weapon used in this game
    /// - Parameters:
    ///   - name: The name of the weapon.
    ///   - attackPoints: The number of damages caused by this weapon.
    init(name: String, attackPoints: Int) {
        self.name = name
        self.attackPoints = attackPoints
    }
}
