//
//  Player.swift
//  rolePlayingGame
//
//  Created by Ludovic HENRY on 11/12/2019.
//  Copyright © 2019 Ludovic HENRY. All rights reserved.
//

import Foundation

/// One of the players.
class Player {
    var name: String
    var characters: [Character]
    enum Mode {
        case showHp, showAp, showAll
    }
    
    /// Initializes one of the players of this game.
    /// - Parameters:
    ///   - name: The name of the player
    ///   - characters: A table that contains his players.
    init(name: String, characters: [Character]) {
        self.name = name
        self.characters = characters
    }
    
    /// One of the player's characters will attack one of the enemy player's characters.
    /// - Parameter ennemy: The enemy player who will be attacked.
    /// - Returns: true if everything went well.
    func attack(ennemy: Player) -> Bool {
        print("👤 \(name)")
        print("🤺Entrez le numéro du personnage qui attaquera")
        showInfos(mode: Mode.showAp)
        let fighter_ = readLine()
        print("🤺Entrez le numéro de l'ènemi que vous souhaitez attaquer")
        ennemy.showInfos(mode: Mode.showHp)
        let characterEnnemy_ = readLine()
        // If the String can be convert to Int continue and returns true
        if let characterEnemy = characterEnnemy_, let fighter = fighter_ {
            if let nbEnemy = Int(characterEnemy), let nbFighter = Int(fighter) {
                if nbEnemy != 0 && nbFighter != 0 {
                    characters[nbFighter - 1].attack(ennemy.characters[nbEnemy - 1])
                    return true
                }
            }
        }
        // If the tests fails return false and print an error
        print("Commande incorrecte")
        return false
    }
    
    /// One of this player's characters will be able to heal himself, or one of his allies.
    /// - Returns: true if everything went well.
    func heal() -> Bool {
        print("👤 \(name)")
        print("💉Entrez le numéro du personnage qui soignera")
        showInfos(mode: Mode.showHp)
        let healer = readLine()
        print("💉Entrez le numéro du personnage que vous souhaitez soigner")
        showInfos(mode: Mode.showHp)
        let ally = readLine()
        // If the String can be convert to Int continue and returns true
        if let healer = healer, let ally = ally {
            if let intHealer = Int(healer), let intAlly = Int(ally) {
                if intHealer != 0 && intAlly != 0 {
                    characters[intHealer - 1].heal(characters[intAlly - 1])
                    return true
                }
            }
        }
        // If the tests fails return false and print an error
        print("Commande incorrecte")
        return false
    }
    
    /// One of the player's characters may or may not open a chest containing a random weapon.
    /// - Parameter weapon: The weapon that one of the characters can equip
    func openChest(weapon: Weapon) {
        print("Quel est le personnage qui ouvrira le coffre")
        showInfos(mode: .showAp)
        let character = readLine()
        if let character = character {
            if let nbrCharacter = Int(character){
                characters[nbrCharacter - 1].openChest(weapon: weapon)
            }
        }        
    }
    
    /// Returns true if the character has lost, or false if he has won.
    func hasLost() -> Bool {
        for character in characters {
            if character.isAlive {
                return false
            }
        }
        return true
    }
    
    /// Display the list of characters alive who can fight.
    /// - Parameter mode: What kind of information should be displayed.
    func showInfos(mode: Mode) {
        var txt = ""
        switch mode {
        case Mode.showHp:
            for (i, character) in characters.enumerated() {
                if character.isAlive {
                    txt += "[\(i+1): 🦹\(character.name) ♥️\(character.healPoints)HP] "
                }
            }
            print(txt)
        case Mode.showAp:
            for (i, character) in characters.enumerated() {
                if character.isAlive {
                    txt += "[\(i+1): 🦹\(character.name) 🗡\(character.weapon.attackPoints)AP] "
                }
            }
            print(txt)
        case Mode.showAll:
            for (i, character) in characters.enumerated() {
                if character.isAlive {
                    txt += "[\(i+1): 🦹\(character.name) \(character.healPoints)HP 🗡\(character.weapon.attackPoints)AP] "
                } else {
                    txt += "[\(i+1): 💀\(character.name) Mort 🗡\(character.weapon.attackPoints)AP] "
                }
            }
            print(txt)
        }
    }
}
