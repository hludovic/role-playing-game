//
//  Character.swift
//  rolePlayingGame
//
//  Created by Ludovic HENRY on 11/12/2019.
//  Copyright © 2019 Ludovic HENRY. All rights reserved.
//

import Foundation

/// One of the player's character.
class Character {
    var healPoints: Int
    var weapon: Weapon
    var name: String
    /// Returns true if this character is alive, or false if he is dead.
    var isAlive: Bool {
        if healPoints <= 0 {
            return false
        } else {
            return true
        }
    }
    
    /// Initializes one of the characters used in this game.
    /// - Parameters:
    ///   - name: The name of che character.
    ///   - weapon: The Weapon used by this character.
    init(name: String, weapon: Weapon) {
        self.name = name
        self.weapon = weapon
        healPoints = 10
    }
    
    /// This character makes an attack against an enemy, and the informations about this action are displayed.
    /// - Parameter enemy: The enemy character who will receive this attack
    func attack(_ enemy: Character) {
        enemy.healPoints -= weapon.attackPoints
        print("⚔️\(name) frappe \(enemy.name) avec \(weapon.name) et lui enlève \(weapon.attackPoints)HP")
    }
    
    /// This character heals an ally.
    /// - Parameter ally: The ally who will be healed.
    func heal(_ ally: Character) {
        let heal = Int.random(in: 1...5)
        ally.healPoints += heal
        print("⚔️\(name) soigne \(ally.name) et lui restaure \(heal)HP")
    }
    
    /// This character opens a chest.
    /// - Parameter weapon: The gun he'll find in the chest.
    func openChest(weapon: Weapon) {
        print("\(name) ouvre le coffre et récupère \(weapon.infos)")
        print("Il remplace \(self.weapon.infos) par \(weapon.infos)")
        self.weapon = weapon
    }
}
