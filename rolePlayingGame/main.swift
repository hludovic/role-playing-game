//
//  main.swift
//  rolePlayingGame
//
//  Created by Ludovic HENRY on 11/12/2019.
//  Copyright © 2019 Ludovic HENRY. All rights reserved.
//

import Foundation

let game = Game()
game.startNewGame()

while !game.isOver() {
    game.round()
}
game.ending()

