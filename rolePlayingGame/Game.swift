//
//  Game.swift
//  rolePlayingGame
//
//  Created by Ludovic HENRY on 11/12/2019.
//  Copyright © 2019 Ludovic HENRY. All rights reserved.
//

import Foundation

class Game {
    /// The list of weapoons used in game. The first  is the default weapon, and it never used in the random chest
    private let weaponList:[Weapon] = [Weapon(name: "Dagger", attackPoints: 4), Weapon(name: "Sling", attackPoints: 3), Weapon(name: "Longsword", attackPoints: 6), Weapon(name: "Greataxe", attackPoints: 7), Weapon(name: "Battleaxe", attackPoints: 7), Weapon(name: "Short Sword", attackPoints: 4)]
    private var rounds: Int
    private var player1: Player?
    private var player2: Player?
    
    /// Initialize a new game. To initialize new players, you will need to startNewGame.
    init() {
        rounds = 0
    }
    
    /// This function initializes the new players and it assigns zero to the variable "rounds".
    func startNewGame() {
        rounds = 0
        player1 = createPlayer(nbr: 1)
        player2 = createPlayer(nbr: 2)
    }
    
    /// Create a new player and returns an object Player.
    /// - Returns: Returs a new Player
    /// - Parameter nbr: the number of the player (1 for the player one, 2 for the player two)
    private func createPlayer(nbr: Int) -> Player {
        print("👤 Joueur \(nbr)")
        // Create a list of three characters
        var tabChars:[Character] = []
        for i in 1...3 {
            var charName: String?
            // While the name is nil or ""
            while charName == nil || charName!.count < 1 {
                print("Entrez le nom de votre personnage n°\(i)")
                charName = readLine()
                while doesThisNameExists(name: charName!, tab: tabChars) {
                    print("⚠️ Ce nom est déjà utilisé")
                    print("Entrez le nom de votre personnage n°\(i)")
                    charName = readLine()
                }
                if let name = charName {
                    tabChars.append(Character(name: name, weapon: weaponList[0]))
                }
            }
        }
        // Create and return a new player with his name and his three characters
        let player = Player(name: "Joueur \(nbr)", characters: tabChars)
        return player
    }
    
    /// Randomly drop a chest and offer the player the opportunity to take the weapon it contains.
    /// - Parameter player: The player who will have a chance to receive a treasure chest.
    private func getRandomChest(for player: Player) {
        // One-in-three chance of having a tresaur chest
        let isWon: Bool = (Int.random(in: 1...3) == 1) ? true : false
        if isWon {
            let nbrWeapon = Int.random(in: 1...(weaponList.count - 1))
            let weapon = weaponList[nbrWeapon]
            print("💎Vous découvrez un coffre au trésor, que faites-vous ?")
            print("[1: 🔓Vous l'ouvrez et récupérez son contennu] [2: 🔒Vous l'abandonnez]")
            let action_ = readLine()
            if let action = action_ {
                switch action {
                case "1":
                    player.openChest(weapon: weapon)
                case "2":
                    print("Vous abandonnez le coffre")
                default:
                    print("Sans avoir pu vous décider, le prochain combat commence")
                }
            }
        }
    }
    
    /// Suggests one players to perform one action
    /// - Parameter players: A table containing the players who will act
    ///  player[0] is the attacker, player[1] is the enemy.
    private func action(players: [Player]) -> Bool {
        if !isOver() {
            print("👤 Tour du \(players[0].name) - Entrez le numéro de l'action à effectuer")
            print("[1: Pour 🗡 attaquer]  [2: Pour 🧪 soigner]")
            let action_ = readLine()
            if let action = action_ {
                if let actionInt = Int(action) {
                    switch actionInt {
                    case 1:
                        var attack = players[0].attack(ennemy: players[1])
                        while !attack {
                            attack = players[0].attack(ennemy: players[1])
                        }
                        return true
                    case 2:
                        var healing = players[0].heal()
                        while !healing {
                            healing = players[0].heal()
                        }
                        return true
                    default:
                        print("Réponse non correcte")
                        return false
                    }
                }
            }
            return false
        }
        return true
    }
    
    /// Returns true if the game is over, or false if the game is not over
    func isOver() -> Bool {
        if let p1 = player1, let p2 = player2 {
            let players = [p1, p2]
            for player in players {
                if player.hasLost() {
                    return true
                }
            }
        }
        return false
    }
    
    /// During a round both players will perform an action.
    /// Before all players actions a safe can appear randomly.
    /// At the end of the round,  the variable "rounds" is incremented.
    func round() {
        if let p1 = player1, let p2 = player2 {
            let players = [p1, p2]
            for (i, player) in players.enumerated() {
                if !isOver() {
                    let x: Int = (i == 0) ? 0 : 1
                    let y: Int = (i == 0) ? 1 : 0
                    getRandomChest(for: player)
                    var turn = action(players: [players[x], players[y]])
                    while turn == false {
                        print("Commande incorrecte")
                        turn = action(players: [players[x], players[y]])
                    }
                }
            }
            rounds += 1
        }
    }
    
    /// When the game is over, you can display the winer and the scores with this function.
    func ending() {
        if let p1 = player1, let p2 = player2 {
            if game.isOver() {
                print("  📯 Jeu terminé en \(rounds) tours 📯")
                if p1.hasLost() {
                    print("  🥇Le vainqueur est \(p2.name)🥇")
                } else {
                    print("  🥇Le vainqueur est \(p1.name)🥇")
                }
                print("👤 \(p1.name)")
                p1.showInfos(mode: .showAll)
                print("👤 \(p2.name)")
                p2.showInfos(mode: .showAll)
            } else {
                print("Le jeu n'est pas terminé")
            }
        }
    }
    
    /// This function checks if a name is already used among the character names
    /// - Parameters:
    ///   - name: The name you want to test
    ///   - tab: You can also check the existence of the name in an optional table of characters.
    /// - Returns: true if this name is already used or false if not.
    private func doesThisNameExists(name: String, tab:[Character]?) -> Bool {
        if let characters = tab {
            for character in characters {
                if name == character.name {
                    return true
                }
            }
        }
        if let p1 = player1 {
            for character in p1.characters {
                if character.name == name {
                    return true
                }
            }
        }
        if let p2 = player2 {
            for character in p2.characters {
                if character.name == name {
                    return true
                }
            }
        }
        return false
    }
}
